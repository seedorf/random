package com.seedorf.randomizer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.seedorf.randomizer.Activities.RandomIntActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button randomIntBtn = (Button) findViewById(R.id.random_int_start_btn);
        randomIntBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startIntent = new Intent(getApplicationContext(), RandomIntActivity.class );
                startActivity(startIntent);
            }
        });
    }
    Intent startIntent = new Intent(this, RandomIntActivity.class );

}
